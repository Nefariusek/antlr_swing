tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0; 
  Integer blockId = 0; 
} 

prog    : (e+=curScope | e+=expr | d+=decl)* -> mainTemplate(name={$e}, deklaracje={$d});
 
curScope: ^(LBR {blockId++; localSymbols.enterScope();} (e+=curScope | e+=expr | d+=decl)* {blockId--; localSymbols.leaveScope();}) -> blockTemplate(name={$e}, deklaracje={$d});

decl  :
        ^(VAR i1=ID) {localSymbols.newSymbol($ID.text + blockId.toString());} -> dek(n={$ID.text + blockId.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> addOperation(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subOperation(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mulOperation(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divOperation(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> setOperation(p1={$i1.text + blockId.toString()},p2={$e2.st})    
        | ID                       -> getOperation(p1={$ID.text})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
    ;
    